# Christie Campus Health Daily Import Files

Christie Campus Health provides outsourced mental health services for eligible Clayton State students. Detailed call reports are transmitted from Christie Campus Health as frequently as daily. This solution retrieves files from Christie Campus Health and prepares data for consumption through Metabase.
