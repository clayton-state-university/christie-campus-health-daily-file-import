#!/bin/sh

source ./config.conf

FRESH_DIR_NAME=fresh
FRESH_DIR=./$FRESH_DIR_NAME/
ARCHIVE_DIR=./archive/
SLEEP_TIME=8
SENDEMAIL=0

log_output(){
  echo "`date`: $1"
}

process_file(){
  log_output "processing file $1..."
  log_output "uploading to Oracle Directory..."
  cp $FRESH_DIR/$1 data.txt

  external-table-uploader-local -p data.txt -d christie_campus_health_daily_reporting_extract
  log_output "done uploading to Oracle Directory..."
  log_output "run the SQL to merge new records into master table"
  sql-runner-local app_christie_campus_daily_extract ./upsert_daily_files.sql
  log_output "done running the sql"

  rm data.txt

}

log_output "beginning script..."
log_output "clearing out fresh/..."

cd fresh/
rm *
log_output "done clearing out fresh/..."

log_output "retrieving fresh files from christie campus health..."


sftp -b ../retrieve.sftp $SFTP_USER@$SFTP_SERVER
cd ..
log_output "finish retrieving fresh files..."

DIFF=$(echo $(diff -r $FRESH_DIR $ARCHIVE_DIR | grep $FRESH_DIR_NAME | awk '{print $4}'))

for FILE in `echo $DIFF`; do 
  process_file $FILE
  
  log_output "sleeping for $SLEEP_TIME s"
  sleep $SLEEP_TIME

  log_output "copying $FILE to $ARCHIVE_DIR"
  cp $FRESH_DIR/$FILE $ARCHIVE_DIR

  SENDEMAIL=1
done

if [ $SENDEMAIL = 1 ]
then 
  log_output "sending email to $RECIPIENTS"
  mail -s "Notification: new CCH file" $RECIPIENTS < email.txt
fi

log_output "done"

